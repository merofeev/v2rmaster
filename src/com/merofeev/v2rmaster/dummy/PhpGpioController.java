package com.merofeev.v2rmaster.dummy;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class PhpGpioController extends Thread {
	private int mId,mState;
	private String mHost;
	
	public PhpGpioController(String host,int id, boolean state)
	{
		mHost = host;
		mId = id;
		mState = state ? 1 : 0;
	}
	
	@Override
	public void run()
	{
		URL url;
		try {
			url = new URL("http://" + mHost + "/led.php?i=" + String.valueOf(mId) + "&s=" + mState);
		} catch (MalformedURLException e) {
			return;
		}
		
		try {
			url.openStream().close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
}
