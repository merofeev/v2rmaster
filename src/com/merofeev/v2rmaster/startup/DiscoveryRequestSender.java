package com.merofeev.v2rmaster.startup;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

class DiscoveryRequestSender extends Thread {
	public enum DiscoveryAction { 
		Connect , Discovery
	}
	
	public interface RequestSendResultListener {
		void RequestSendResult(boolean success);
	}
	private RequestSendResultListener mResultListener = null;
	
	DatagramSocket socket;
	String mIp;
	private boolean mSuccess;
	
	DiscoveryAction mAction;
	public DiscoveryRequestSender(DiscoveryAction action,String ip) throws SocketException, UnknownHostException{
		CreateSocket();
		mAction = action;
		if(ip.isEmpty()) ip = "255.255.255.255";
		mIp = ip;
	}

	private void CreateSocket() throws SocketException, UnknownHostException
	{
		if(socket!=null) return;
		try {
			socket = new DatagramSocket(0, InetAddress.getByName("0.0.0.0"));
		} catch (SocketException e) {
			e.printStackTrace();
			throw e;
		} catch (UnknownHostException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void run() {
		mSuccess = true;
		String action="";
		switch(mAction) {
			case Connect:
				action = "connect";
				break;
			case Discovery:
				action = "discovery";
				break;
		}
		
		String request = "{ \"version\": 1 , \"action\" : \"" + action + "\" }";
		byte[] bPacket = request.getBytes();
		
		DatagramPacket packet = new DatagramPacket(bPacket, bPacket.length);
		packet.setPort(3002);
		try {
			packet.setAddress( InetAddress.getByName(mIp) );
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
			mSuccess = false;
		}
		
		try {
			socket.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
			mSuccess = false;
		}
		socket.close();
		if(mResultListener != null) {
			mResultListener.RequestSendResult(mSuccess);
		}
	}
	
	public void onSendResult(RequestSendResultListener listener) {
		mResultListener = listener;
	}


	

}