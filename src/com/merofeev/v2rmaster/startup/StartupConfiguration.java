package com.merofeev.v2rmaster.startup;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.merofeev.v2rmaster.R;
import com.merofeev.v2rmaster.V2RMain;
import com.merofeev.v2rmaster.startup.AskDeviceIpDialog.ManualDeviceIpListener;
import com.merofeev.v2rmaster.startup.DiscoveryRequestSender.RequestSendResultListener;



public class StartupConfiguration extends Activity  implements ManualDeviceIpListener{
	
	private final String mDevicesKey = "V2RDevices"; 
	private class DiscoveryResponseListener extends Thread {
		DatagramSocket socket;
		public DiscoveryResponseListener() {
			CreateSocket();
		}

		private void CreateSocket()
		{
			if(socket!=null) return;
			try {
				socket = new DatagramSocket(3002, InetAddress.getByName("0.0.0.0"));
				socket.setBroadcast(true);
			} catch (SocketException e) {
				e.printStackTrace();
				finish();
			} catch (UnknownHostException e) {
				e.printStackTrace();
				finish();
			}
		}

		@Override
		public void run() {
			while(!Thread.interrupted()) {
				DatagramPacket packet = new DatagramPacket(new byte[500], 500);
				try {
					socket.receive(packet);
					InetAddress address = packet.getAddress();
					
					String name = new String( packet.getData() , "UTF-8" );
					if(! name.startsWith("!") ) continue;
					name = name.substring(1);
					
					mDevices.add(new Device(address.getHostAddress(),name));
					activity.runOnUiThread(new Runnable(){				
						@Override
						public void run() {
							updateDeviceSpinner();
						}
					});
					
				} catch (IOException e) {
				}
				
			}
			socket.close();
		}
		
	   @Override
	   public void interrupt(){
	     super.interrupt();  
	     this.socket.close();
	   }
		
	}
	private Spinner mSpinnerSelectDecoder,mSpinnerSelectDevice;
	private ImageButton mButtonRefresh;
	private Button mButtonConnect;
	private ArrayList<Device> mDevices;
	private DiscoveryResponseListener mResponseListener=null;
	private DiscoveryRequestSender mRequestSender = null;
	private Activity activity;
	
	private String[] getSupportedDecoders() {
		return new String[] {"System" , "FFmpeg" };
	}
	
	private void sendDiscoveryRequest() {
		mDevices.clear();
		if (mRequestSender != null) {
			if ( mRequestSender.isAlive() ) return;
			mRequestSender = null;
		}
		try {
			mRequestSender = new DiscoveryRequestSender(DiscoveryRequestSender.DiscoveryAction.Discovery,"");
		} catch (SocketException e) {
			e.printStackTrace();
			finish();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			finish();
		}
		mRequestSender.start();
	}
	
	
	private void updateDeviceSpinner() {
		ArrayList<String> items = new ArrayList<String>();
		for(int i=0;i<mDevices.size();i++) {
			items.add(mDevices.get(i).toString() );
		}
		items.add(getString(R.string.manual_setup));
		
		ArrayAdapter<String> aItems = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,items);
		mSpinnerSelectDevice.setAdapter(aItems);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;
		setContentView(R.layout.startup);
		
		mSpinnerSelectDecoder = (Spinner) findViewById(R.id.spnDecoder);
		mSpinnerSelectDevice = (Spinner) findViewById(R.id.spnDevice);
		mButtonRefresh = (ImageButton) findViewById(R.id.btnRefresh); 
		mButtonConnect = (Button) findViewById(R.id.btnConnect);
		
		ArrayAdapter<String> aDecoders = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, getSupportedDecoders());
		mSpinnerSelectDecoder.setAdapter(aDecoders);
		mResponseListener = new DiscoveryResponseListener();
		
		if(savedInstanceState == null) {
			mDevices = new ArrayList<Device>();
			sendDiscoveryRequest();
		}
		else {
			mDevices = savedInstanceState.getParcelableArrayList(mDevicesKey);
			
		}
		
		updateDeviceSpinner();
		
		mButtonRefresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				sendDiscoveryRequest();
			}
			
		});
		
		mButtonConnect.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View view) {
				int dPos = mSpinnerSelectDevice.getSelectedItemPosition();
				if( dPos >= mDevices.size()) {
					new AskDeviceIpDialog().show(getFragmentManager(), "get_ip");
					return;
				}
				
				Device dev = mDevices.get(dPos);
				connectTo(dev);
			}
			
		});
		
	}

	private boolean isFFmpegSelected() {
		String itm = (String)mSpinnerSelectDecoder.getSelectedItem();
		return itm == "FFmpeg"; 
	}
	protected void switchToMainActivity(final Device device) {
		this.runOnUiThread(new Runnable(){

			@Override
			public void run() {
				Intent intent = new Intent(activity,V2RMain.class);
				intent.putExtra(V2RMain.intentAddress, device.address);
				intent.putExtra(V2RMain.intentUseFFmpeg, isFFmpegSelected());
				activity.startActivity(intent);
				
			}
			
		});
		
	}
	
	protected void connectTo(Device dev) {
		final Device device = dev; 
		final ProgressDialog dialog = new ProgressDialog(this);
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
		dialog.setMessage(String.format(getString(R.string.connecting_to) , dev.toString()));
		dialog.show();
		DiscoveryRequestSender sender = null;
		try {
			sender = new DiscoveryRequestSender(DiscoveryRequestSender.DiscoveryAction.Connect, dev.address);
			sender.onSendResult(new RequestSendResultListener() {
				@Override
				public void RequestSendResult(boolean success) {
					dialog.dismiss();
					if(success) {
						switchToMainActivity(device);
					}
					else {
						Toast
							.makeText(activity, String.format(getString(R.string.conection_failed), device.toString()), Toast.LENGTH_LONG)
								.show();
					}
						
				}
				
			});
			sender.start();
		} catch (SocketException e) {
			e.printStackTrace();
			finish();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			finish();
		}
		
	}

	@Override
	protected void onSaveInstanceState(Bundle bundle) { 
		super.onSaveInstanceState(bundle);
		bundle.putParcelableArrayList(mDevicesKey, mDevices);

	}
	@Override
	protected void onPause() {
		super.onPause();
		mResponseListener.interrupt();
		try {
			mResponseListener.join();
			mResponseListener = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(mResponseListener == null) {
			mResponseListener = new DiscoveryResponseListener();
		}
		mResponseListener.start();
	}

	@Override
	public void onDialogIpEntered(String ip) {
		connectTo(new Device(ip, ""));
	}
}
