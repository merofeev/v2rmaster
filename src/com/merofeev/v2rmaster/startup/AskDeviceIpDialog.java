package com.merofeev.v2rmaster.startup;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.InputType;
import android.widget.EditText;

import com.merofeev.v2rmaster.R;

public class AskDeviceIpDialog extends DialogFragment {
    public interface ManualDeviceIpListener {
        public void onDialogIpEntered(String ip);
    }
    ManualDeviceIpListener listener = null;
    
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.enter_device_ip);

		final EditText ip = new EditText(this.getActivity());
		ip.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
		builder.setView(ip);
		
		builder.setPositiveButton(android.R.string.ok, new OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listener.onDialogIpEntered(ip.getText().toString());
			}
		});
		
		builder.setNegativeButton(android.R.string.cancel, new OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
			
		});
		
		return builder.create();
	}
	 @Override
	 public void onAttach(Activity activity) {
		 super.onAttach(activity);
		 listener = (ManualDeviceIpListener) activity;
	 }

}
