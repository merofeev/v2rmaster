package com.merofeev.v2rmaster.startup;


import android.os.Parcel;
import android.os.Parcelable;

public class Device implements Parcelable{
	public String address,name;
	public Device(String address, String name) {
		this.address = address;
		this.name = name;
	}
	public Device(Parcel parcel) {
		address = parcel.readString();
		name = parcel.readString();
	}
	public String toString() {
		return name + "@" + address;
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(address);
		dest.writeString(name);
	}
}