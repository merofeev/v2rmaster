package com.merofeev.v2rmaster;


import java.net.InetAddress;
import java.net.UnknownHostException;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;


import com.merofeev.v2rmaster.VideoStreamReceiver.OnSourceAddressChangeListener;
import com.merofeev.v2rmaster.dummy.PhpGpioController;

public class V2RMain extends Activity{
	VideoStreamReceiver mVideoStream;
	SurfaceView mSurface;
	InetAddress mV2rAddress = null;
	Boolean mUseFFmpeg;
	public static final String intentUseFFmpeg = "UseFFmpeg";
	public static final String intentAddress = "Addrress";
	Activity activity;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Intent intent = this.getIntent();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mSurface = (SurfaceView) findViewById(R.id.outputSurface);
		mSurface.setVisibility(View.GONE);

		mUseFFmpeg = intent.getBooleanExtra(intentUseFFmpeg,false);
		try {
			mV2rAddress = InetAddress.getByName( intent.getStringExtra(intentAddress) );
		} catch (UnknownHostException e) {
			finish();
		}
		
		activity = this;
		final ActionBar actionBar = getActionBar();

		mSurface.setOnLongClickListener(new OnLongClickListener(){

			@Override
			public boolean onLongClick(View arg0) {
				if(actionBar.isShowing())
					actionBar.hide();
				else
					actionBar.show();
				return true;
			}

		});

		mSurface.setVisibility(View.VISIBLE);
		mVideoStream = new VideoStreamReceiver(640,480,mSurface,mUseFFmpeg,3000);
		mVideoStream.setOnSourceAddressChangeListener(new OnSourceAddressChangeListener(){
			public void SourceAddressChanged(InetAddress newAddress) {		
				activity.runOnUiThread(new Runnable(){
					@Override
					public void run() {
						Toast
							.makeText(activity, String.format(getString(R.string.got_video_stream), mV2rAddress.getHostAddress()) , Toast.LENGTH_LONG)
								.show();
					}

				});
			}
		});
		
		setLedButtons();
	}


	private void setLedButtons() {
		ToggleButton red = (ToggleButton) findViewById(R.id.toggleButtonRed);
		red.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton b, boolean state) {
				if(mV2rAddress!=null)
					new PhpGpioController(mV2rAddress.getHostAddress(),74,state).start();
			}

		});

		ToggleButton green = (ToggleButton) findViewById(R.id.toggleButtonGreen);
		green.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton b, boolean state) {
				if(mV2rAddress!=null)
					new PhpGpioController(mV2rAddress.getHostAddress(),73,state).start();
			}

		});	

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}


}
