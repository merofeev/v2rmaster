package com.merofeev.v2rmaster;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingDeque;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class VideoStreamReceiver implements SurfaceHolder.Callback {
	private int mVideoWidth, mVideoHeight;
	private SurfaceView mSurface;
	private int mPort;
	private InetAddress sourceAddress = null;
	private boolean useFFmpeg = false;

	private  BlockingQueue<DatagramPacket> mPacketQueue;
	private  ConcurrentLinkedQueue<DatagramPacket> mEmptyPacketQueue;
	private PlayerThread mPlayer = null;
	private FFPlayerThread mFFPlayer = null;
	private ReceiverThread mReceiver = null;

	static final int  BUFFER_SIZE = 1024 * 66;
	static final int BUFFER_NUMBER = 3;


	public VideoStreamReceiver(int width,int height, SurfaceView surface,boolean ffmpeg, int port) {
		mVideoWidth = width;
		mVideoHeight = height;
		mSurface = surface;
		mPort = port;
		useFFmpeg = ffmpeg;

		initQueue();
		mSurface.getHolder().addCallback(this);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		resizeSurface();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if(useFFmpeg && mFFPlayer==null)
		{
			mFFPlayer = new FFPlayerThread(holder.getSurface());
			mFFPlayer.start();
		}
		
		if(!useFFmpeg && mPlayer==null)
		{
			mPlayer = new PlayerThread(holder.getSurface());
			mPlayer.start();
		}
		
		if (mReceiver == null) {
			try {
				mReceiver = new ReceiverThread();
				mReceiver.start();
			} catch (Exception e) {
				e.printStackTrace();
			} 
		}
			
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mPlayer != null) {
			mPlayer.interrupt();
			mPlayer = null;
		}
		if (mFFPlayer != null) {
			mFFPlayer.interrupt();
			mFFPlayer = null;
		}
		if( mReceiver != null ){
			mReceiver.interrupt();
			mReceiver = null;
		}
	}

	private void initQueue() {
		mPacketQueue = new  LinkedBlockingDeque<DatagramPacket>();
		mEmptyPacketQueue = new ConcurrentLinkedQueue<DatagramPacket>();

		for(int i=0;i<BUFFER_NUMBER;i++)
		{
			byte[] data = new byte[BUFFER_SIZE];
			DatagramPacket packet = new DatagramPacket(data, BUFFER_SIZE);
			mEmptyPacketQueue.add(packet);
		}
		assert( ( mPacketQueue.size() + mEmptyPacketQueue.size() ) == BUFFER_NUMBER );
	}

	private void resizeSurface() {
		int parentWidth = ((View)mSurface.getParent()).getWidth();
		int parentHeight = ((View)mSurface.getParent()).getHeight();


		int computedWidth = (int) (((float)mVideoHeight / (float)mVideoWidth) * (float)parentWidth);
		int computedHeight = (int) (((float)mVideoWidth / (float)mVideoHeight) * (float)parentHeight);

		if(computedHeight > parentHeight)
		{
			computedHeight = parentHeight;
		}
		else
		{
			computedWidth = parentWidth;
		}

		//Get the SurfaceView layout parameters
		android.view.ViewGroup.LayoutParams lp = mSurface.getLayoutParams();



		//Set the width of the SurfaceView to the width of the screen
		lp.width = computedWidth;
		lp.height = computedHeight;

		//Set the height of the SurfaceView to match the aspect ratio of the video 
		//be sure to cast these as floats otherwise the calculation will likely be 0


		//Commit the layout parameters
		mSurface.setLayoutParams(lp);    

	}

	private class ReceiverThread extends Thread{
		DatagramSocket receive_socket=null;
		
		public ReceiverThread() throws SocketException, UnknownHostException{
			CreateSocket();
		}

		private void CreateSocket() throws SocketException, UnknownHostException
		{
			if(receive_socket!=null) return;
			try {
				receive_socket = new DatagramSocket(mPort, InetAddress.getByName("0.0.0.0"));
			} catch (SocketException e) {
				e.printStackTrace();
				throw e;
			} catch (UnknownHostException e) {
				e.printStackTrace();
				throw e;
			}
		}

		@Override
		public void run() {
			while(!Thread.interrupted()) {
				assert( ( mPacketQueue.size() + mEmptyPacketQueue.size() ) == BUFFER_NUMBER );
				DatagramPacket packet = mEmptyPacketQueue.poll();

				if(packet==null){
					Log.v("h264_perf", "Dropping packet");
					packet =  mPacketQueue.poll();
				}
				if(packet==null)
					continue;
				
				try {
					receive_socket.receive(packet);
				} catch (IOException e) {
					e.printStackTrace();
				}
				InetAddress newAddress = packet.getAddress();
				if(sourceAddress==null || !newAddress.equals(sourceAddress)) {
					sourceAddress = newAddress;
					if(sourceAddressChanged!=null)
						sourceAddressChanged.SourceAddressChanged(newAddress);

				}
					mPacketQueue.add(packet);
					assert( ( mPacketQueue.size() + mEmptyPacketQueue.size() ) == BUFFER_NUMBER );
			}
			receive_socket.close();
		}

	}

	private class FFPlayerThread extends Thread{
		private Surface mSurface;
		H264Decoder ffdec=null;
		
		static final int INPUT_BUFFER_SIZE = 1024*100;
		static final long MAX_DELAY = 200;

		public FFPlayerThread(Surface surface) {
			this.mSurface = surface;

		}


		private void initDecoder() {
			if(ffdec!=null)
			{
				ffdec.releaseSurface();
				ffdec=null;
			}
			ffdec = new H264Decoder(H264Decoder.COLOR_FORMAT_RGB565LE);
			ffdec.setSurface(mSurface);
		}	
		
		@Override
		public void run() {
			this.initDecoder();
			
			ByteBuffer inputBuffer = ByteBuffer.allocateDirect(INPUT_BUFFER_SIZE);

			long delay = 0;
			byte[] type = new byte[1];

			while (!Thread.interrupted()) {
				inputBuffer.clear();
				int packetSize = getPacket(inputBuffer,type);
				if( (packetSize>0) && (delay < MAX_DELAY) )
				{
					ffdec.consumeNalUnitsFromDirectBuffer(inputBuffer, packetSize, System.currentTimeMillis());
				}
				else if(delay >= MAX_DELAY)
				{
					flushQueue();
					this.initDecoder();
					delay = 0;
					Log.v("h264_perf", "FF reset!");
				}

				if(ffdec.isFrameReady())
				{

					long res = ffdec.render(System.currentTimeMillis() - MAX_DELAY);
					delay = System.currentTimeMillis() - res;
					Log.v("h264_perf","FF produced frame!!! w:" + String.valueOf(ffdec.getWidth()) + " delay:" + String.valueOf(delay)); 

				}
			}
			ffdec.releaseSurface();

		}
	}


	private int getPacket(ByteBuffer buf,byte[] type)
	{
		DatagramPacket packet = mPacketQueue.poll();
		if(packet==null)
			return 0;
		byte[] data = packet.getData();
		buf.put(data, 0, packet.getLength());
		type[0] = data[4];

		mEmptyPacketQueue.add(packet);
		assert( ( mPacketQueue.size() + mEmptyPacketQueue.size() ) == BUFFER_NUMBER );
		return packet.getLength();
	}
	

	private void flushQueue()
	{
		DatagramPacket packet = mPacketQueue.poll();
		while(packet!=null)
		{
			mEmptyPacketQueue.add(packet);
			packet = mPacketQueue.poll();
		}
		assert( ( mPacketQueue.size() + mEmptyPacketQueue.size() ) == BUFFER_NUMBER );
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private class PlayerThread extends Thread {

		private MediaCodec mDecoder;
		private Surface mSurface;

		public PlayerThread(Surface surface) {
			this.mSurface = surface;

		}

		@Override
		public void run() {
			mDecoder = MediaCodec.createDecoderByType("video/avc");

			if (mDecoder == null) {
				Log.e("DecodeActivity", "Can't find appropriate decoder!");
				return;
			}

			MediaFormat format = MediaFormat.createVideoFormat("video/avc", mVideoWidth, mVideoHeight);
	
			
			mDecoder.configure(format, mSurface, null, 0);
			mDecoder.start();

			ByteBuffer[] inputBuffers = mDecoder.getInputBuffers();
			mDecoder.getOutputBuffers();

			BufferInfo info = new BufferInfo();
			boolean isEOS = false;
			long startMs = System.currentTimeMillis();
			byte[] type = new byte[1];

			long firstPacketTS = 0;
			long firstFrameTS = 0;
			int inIndex=-1;
			boolean decoderConfigured = false;
			
			boolean needInBuffer = true;

			while (!Thread.interrupted()) {
				if (!isEOS) {

					if(needInBuffer)
						inIndex = mDecoder.dequeueInputBuffer(10000);

					if (inIndex >= 0) {
						ByteBuffer buffer = inputBuffers[inIndex];
						buffer.clear();
						int sampleSize = getPacket(buffer,type);

						needInBuffer = true;

						byte[] cfg_codes = {0x67,0x68,0x65};
						if( sampleSize == 0 )
						{
							needInBuffer = false;
						}
						else if (sampleSize < 0) {
							Log.d("DecodeActivity", "InputBuffer BUFFER_FLAG_END_OF_STREAM");
							mDecoder.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
							isEOS = true;
						} else {
							int flag = 0;

							if(firstPacketTS==0)
							{
								firstPacketTS = System.currentTimeMillis();
							}

							for(int j=0;j<cfg_codes.length;j++)
							{
								if(cfg_codes[j]==type[0])
								{
									flag = MediaCodec.BUFFER_FLAG_CODEC_CONFIG;
									decoderConfigured = true;
									break;
								}
							}
							if(!decoderConfigured) {
								needInBuffer=false;
							}
							else {
								mDecoder.queueInputBuffer(inIndex, 0, sampleSize, System.currentTimeMillis() - startMs , flag);
							}

						}
					}
				}

				int outIndex = mDecoder.dequeueOutputBuffer(info, 10000);
				switch (outIndex) {
				case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
					Log.d("DecodeActivity", "INFO_OUTPUT_BUFFERS_CHANGED");
					mDecoder.getOutputBuffers();
					break;
				case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
					Log.d("DecodeActivity", "New format " + mDecoder.getOutputFormat());
					break;
				case MediaCodec.INFO_TRY_AGAIN_LATER:
					Log.d("DecodeActivity", "dequeueOutputBuffer timed out!");
					break;
				default:
					//ByteBuffer buffer = outputBuffers[outIndex];
					//Log.v("DecodeActivity", "We can't use this buffer but render it due to the API limit, " + buffer);

					// We use a very simple clock to keep the video FPS, or the video
					// playback will be too fast
					while (info.presentationTimeUs / 1000 > System.currentTimeMillis() - startMs) {
						try {
							sleep(10);
						} catch (InterruptedException e) {
							e.printStackTrace();
							break;
						}
					}
					mDecoder.releaseOutputBuffer(outIndex, true);
					if(firstFrameTS==0)
					{
						firstFrameTS=System.currentTimeMillis();
						Log.v("h264_perf", String.format("performance %f\n", (firstFrameTS - firstPacketTS) / 1000.0f));
					}
					break;
				}

				// All decoded frames have been rendered, we can stop playing now
				if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
					Log.d("DecodeActivity", "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
					break;
				}
			}

			mDecoder.stop();
			mDecoder.release();

		}
	}
	public interface OnSourceAddressChangeListener {
		public void SourceAddressChanged(InetAddress newAddress);
	}
	private OnSourceAddressChangeListener  sourceAddressChanged = null;

	public void setOnSourceAddressChangeListener(OnSourceAddressChangeListener l) {
		sourceAddressChanged = l;
		if(sourceAddress!=null)
			l.SourceAddressChanged(sourceAddress);
	}
}


